###Fund Finder for http://portal.amfiindia.com/

### Installation

#### Install meteor

`curl https://install.meteor.com/ | sh`

#### Install mongodb

https://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/

#### Install node with nvm

https://github.com/creationix/nvm

### Starting the development server

```
#export mongo url
export MONGO_URL="mongodb://localhost:27017/fund_finder" 

# start meteor server
meteor
```