Tracker.autorun ->
  searchType = Session.get "searchType"
  query = Session.get "searchValue"
  limit = Session.get "searchLimit"
  schemeCode = Session.get "schemeCode"
  range = Session.get "slider"
  if searchType is "Plans"
    if query isnt ''
      Meteor.subscribe('plansSearch', query, limit)
    else
      Meteor.subscribe("allPlans",schemeCode, range,limit)
  else if searchType is "Funds"
    if query isnt ''
      Meteor.subscribe('fundsSearch', query, limit)
    else
      Meteor.subscribe("allFunds")
  else if searchType is "SchemeTypes"
    if query isnt ''
      Meteor.subscribe('schemesSearch', query, limit)
    else
      Meteor.subscribe("allSchemeTypes")
