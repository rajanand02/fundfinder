Template.search.onRendered ->
  #initialize material select
  $('select').material_select()
  
  #initialize nouislider
  @$('#slider').noUiSlider(
    start: Session.get('slider')
    connect: true
    range:
      'min': 0
      'max': 200)
  .on 'change', (ev, val) ->
    Session.set 'slider', [
      Math.round(val[0])
      Math.round(val[1])
    ]

Template.search.helpers
  slider: ->
    Session.get "slider"

Template.SchemeTypes.helpers
  # all schemes
  schemeTypes: ->
    SchemeTypes.find()

Template.Plans.helpers
  # plans cursor
  plans: ->
    if Session.get "searchValue"
      Plans.find({}, { sort: [["score", "desc"]] })
    else
      Plans.find({}, { sort: { netAssetValue: -1 } })
      
  # check if plans are available or not
  isPlanAvailable: ->
    count = Plans.find().count()
    if count > 0
      true
    else
      false

Template.Funds.helpers
  # funds cursor
  funds: ->
    Funds.find()


Template.search.events
  # search plans on keyup
  "keyup #search":(e,t) ->
    searchValue =  t.$(e.currentTarget).val()
    if searchValue.length > 2
      Session.set "searchValue", searchValue
      Session.set "schemeCode", ""
    else
      Session.set "searchValue", ""

  # get current search type based on select value
  "change #searchType": (e,t)->
    type = t.$(e.currentTarget).val()
    if type isnt ''
      Session.set "searchType", type

  # get scheme code for individual scheme search
  "keypress #schemeCode":(e,t)->
    if e.which is 13
      code = $(e.currentTarget).val()
      if code isnt "" and code.length is 6
        Session.set "schemeCode", parseInt(code)
        $(e.currentTarget).val('')
      else
        Session.set "schemeCode", ""

Template.Plans.events
  # add pagination
  "click #load-more":(e,t)->
    currentLimt = Session.get "searchLimit"
    limit = currentLimt + 10
    Session.set "searchLimit", limit


