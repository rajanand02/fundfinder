# send all plans or one plans based on subscription type
Meteor.publish "allPlans",(schemeCode,range,limit) ->
  if schemeCode
    Plans.find({schemeCode: schemeCode})
  else if range[0] isnt 0 or range[1] isnt 200
    Plans.find(
      { netAssetValue: {  $gt: range[0], $lt: range[1]  } }
      { limit: limit  }
      { sort: { netAssetValue: -1 } }
    )
  else
    Plans.find({}
      { limit: limit  }
      { sort: netAssetValue: -1  }
    )

# all funds publication
Meteor.publish "allFunds", () ->
  Funds.find({}, { sort: { name: 1 } })

# publish all schemeTypes
Meteor.publish "allSchemeTypes", () ->
  SchemeTypes.find({}, { sort: { name: 1 } })

# text search only for plans
Meteor.publish "plansSearch", (searchValue, limit) ->
  cursor = Plans.find(
    { $text: {$search: searchValue} }
    {
      fields: {
        score: { $meta: "textScore" }
      }
      sort: {
        score: { $meta: "textScore" }
      }
      limit: limit
    }
  )
  cursor

# text search for funds and associated plans
Meteor.publish "fundsSearch",(searchValue, limit) ->
  funds = Funds.find(
    { $text: { $search: searchValue } }
  {
    fields: {
      score: { $meta: "textScore" }
    },
    sort: {
      score: { $meta: "textScore" }
    }
  })
  if funds.count()> 0
    fundIds = funds.map((fund) -> fund._id._str)
    plans = Plans.find({fundId: { $in: fundIds } }, { limit: limit })
    return [
      plans
      funds
    ]

# text search for schemes, funds and plans associated to result schemes
Meteor.publish "schemesSearch", (searchValue, limit) ->
  schemes = SchemeTypes.find(
    { $text: { $search: searchValue} }
  {
    fields: {
      score: { $meta: "textScore" }
    }
    sort: {
      score: { $meta: "textScore" }
    }
  })
  schemesIds = schemes.map((scheme) -> scheme._id._str)
  funds = Funds.find({ schemeId: { $in: schemesIds } })
  plans = Plans.find({ schemeId: { $in: schemesIds } }, { limit: limit })
  return [
    schemes
    funds
    plans
  ]
